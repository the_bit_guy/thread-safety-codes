package reservation;

public class BusReservation implements Runnable{ 
	
	private int totalSeatsAvailable = 2;
	
	private int getTotalSeatsAvailable(){
		return totalSeatsAvailable;
	}

	public synchronized boolean bookTickets(int seats, String name){
		System.out.println("Welcome To HappyBus "+Thread.currentThread().getName());
		System.out.println("No. of seats available are: "+this.getTotalSeatsAvailable());
		
		if(seats > this.getTotalSeatsAvailable()){
			return false;
		}else{
			totalSeatsAvailable = totalSeatsAvailable - seats;
			return true;
		}
	}
	@Override
	public void run() {
		
		PassangerThread pt = (PassangerThread)Thread.currentThread();
		boolean ticketsBooked = this.bookTickets(pt.getSeatsNeeded(), pt.getName());
		if(ticketsBooked == true)
			System.out.println("CONGRATS!!!"+pt.getName()+" The nunber of seats requested ("+pt.getSeatsNeeded()+") are BOOKED");
		else
			System.out.println("SORRY "+pt.getName()+" The nunber of seats requested ("+pt.getSeatsNeeded()+") are not available");
	}

}
