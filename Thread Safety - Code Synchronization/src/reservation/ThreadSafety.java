package reservation;

public class ThreadSafety {

	public static void main(String[] args) {
		
		BusReservation br = new BusReservation();
		
		PassangerThread pt1  =  new PassangerThread(2, br, "Ramdev");
		PassangerThread pt2 = new PassangerThread(2, br, "dyuti");
		
		pt1.start();
		pt2.start();
		
	}

}
